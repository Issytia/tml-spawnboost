using Terraria;
using Terraria.ModLoader;
using Terraria.ModLoader.Config;
using System.ComponentModel;

namespace IssySpawnBoost {
	public class IssySpawnBoost : Mod {}

	public class IssyNPC : GlobalNPC {
		static float RateMult { get { return IssyConfig.Instance.Rate; } }
		static float CapMult { get { return IssyConfig.Instance.Cap; } }

		public override void EditSpawnRate(Player player, ref int rate, ref int cap) {
			rate = (int)(rate / RateMult);
			cap = (int)(cap * CapMult);
		}
	}

	[Label("Server config")]
	class IssyConfig : ModConfig {
		public override ConfigScope Mode => ConfigScope.ServerSide;
		public static IssyConfig Instance => ModContent.GetInstance<IssyConfig>();

		[Label("Spawn rate multiplier")]
		[Increment(0.1f)]
		[Range(0.1f, 10f)]
		[DefaultValue(1f)]
		[Slider]
		public float Rate;

		[Label("Spawn cap multiplier")]
		[Increment(0.1f)]
		[Range(0.1f, 10f)]
		[DefaultValue(1f)]
		[Slider]
		public float Cap;
	}
}
